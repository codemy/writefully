class Post < ApplicationRecord
  enum status: { draft: 0, published: 1 } do
    event :publish do
      after do
        self.published_at = DateTime.now
        self.save
      end

      transition :draft => :published
    end

    event :unpublish do
      transition :published => :draft
    end
  end

  attr_accessor :state_event, :remove_cover_picture

  after_save :trigger_state, if: :state_event
  private def trigger_state
    send(state_event) if send(:"can_#{state_event}?")
  end

  after_save :purge_cover_picture, if: :remove_cover_picture
  private def purge_cover_picture
    cover_picture.purge_later
  end

  has_many :comments

  has_one_attached :cover_picture

  has_many :taggings
  has_many :tags, through: :taggings
end
